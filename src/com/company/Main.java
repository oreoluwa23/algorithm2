package com.company;

public class Main {

    public static void main(String[] args) {
        String a = "12";
        String b = "10";
        System.out.println(addStrings(a, b));
        System.out.println(tennisScore(new int[]{1,2,3,4,5}, new int[]{5,4,3,2,1}));
    }

    public static String addStrings(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        int i = num1.length() - 1;
        int j = num2.length() - 1;
        while (i > -1 || j > -1) {
            int sum = carry + (i < 0 ? 0 : num1.charAt(i--) - 48);
            sum += j < 0 ? 0 : num2.charAt(j--) - 48;
            sb.append(sum % 10);
            carry = sum / 10;
        }
        return sb.append(carry == 1 ? "1" : "").reverse().toString();
    }

    public static  int[] tennisScore(int[] player1, int[] player2) {
        int player1Score = 0;
        int player2Score = 0;
        for (int i = 0; i < player1.length; i++) {
            if (player1[i] > player2[i]) {
                player1Score++;
            } else if (player1[i] < player2[i]) {
                player2Score++;
            }
        }
        System.out.println("player 1: " + player1Score + "\nPlayer 2: " + player2Score);
        return new int[]{player1Score, player2Score};
    }


}
